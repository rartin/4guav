#!/bin/bash

# Copyright Peter Burke 8/15/2019

# define functions first



# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

#***********************END OF FUNCTION DEFINITIONS******************************
start_time="$(date -u +%s)"

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install getrssi.sh on Pi and set it up for you."
echo "See README in 4guav gitlab repo for documentation."


echo "Starting..."


time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
time sudo apt-get -y upgrade # 3.5 min

time sudo apt-get install links2 -y; 


cd /home/pi
wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/getrssi.sh -O /home/pi/getrssi.sh 
sudo chmod 777 *.sh


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Now download the autostart scripts for ssh to AWS"
if [ ! -d "/home/pi/log" ] 
then
    echo "Directory /home/pi/log does not exist yet. Making it." 
    sudo -u pi mkdir /home/pi/log
    echo "Made /home/pi/log" 
fi
cd /home/pi/startupscripts

wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/start_getrssi.sh -O /home/pi/startupscripts/start_getrssi.sh 
wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/autostart_getrssi.sh -O /home/pi/startupscripts/autostart_getrssi.sh 

echo "Did download the autostart scripts for getrssi.sh"

sudo chmod 777 *.sh


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "adding autostart_sshtoAWS.sh to /etc/rc.local"
# Add these lines to /etc/rc.local
#sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_mavlinkrouter.sh'
#sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_sshtoAWS.sh'

# If line already was added delete it.
sudo sed -i -n '/autostart_getrssi.sh/!p' /etc/rc.local

a="
sudo -H -u pi /bin/bash -c '/home/pi/startupscripts/autostart_getrssi.sh'
exit 0
"
# delete exit on last line of /etc/rc.local
sudo sed -i '/exit 0/d'  /etc/rc.local
# append a to end
sudo sh -c "echo '$a'>>/etc/rc.local"
sudo chown root:root /etc/rc.local
sudo chmod 755 /etc/rc.local
echo "added autostart_getrssi.sh to /etc/rc.local"

# Now clean up extra spaced lines:
tmpfile=$(mktemp)
sudo awk '!NF {if (++n <= 1) print; next}; {n=0;print}' /etc/rc.local > "$tmpfile" && sudo mv "$tmpfile" /etc/rc.local
sudo chmod 777 /etc/rc.local


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"


end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

