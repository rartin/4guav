#!/bin/bash

set -e
set -x

TITLE=getrssi
MAVLINKROUTER_HOME=$HOME/startupscripts
SCRIPT=$MAVLINKROUTER_HOME/start_getrssi.sh
LOG=$MAVLINKROUTER_HOME/autostart_getrssi.log

# autostart for mavproxy
(
    set -e
    set -x

    date
    set

    cd $MAVLINKROUTER_HOME
    screen -L -d -m -S "$TITLE" -s /bin/bash $SCRIPT
) >$LOG 2>&1
exit 0
