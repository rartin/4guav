#!/bin/bash

#set -e
#set -x

# start mavlink router hopefully /etc/mavlink-router/main.conf loads:

HEADER="Date\tTime\tStatus\tRSSI (dBm)\tSNR (dB)\tChannel\tTemperature (C)"
echo -e $HEADER >> ~/rssihistory.txt

while true; do
    # links2 -dump 192.168.1.1/diagnostics |grep dBm | xargs echo -n >> ~/rssihistory.txt
    # echo -n " " >> ~/rssihistory.txt


    LINKSOUT=$(links2 -dump 192.168.1.1/diagnostics)
    # Need to parse this so that if there is an error, it gets detected.

    #For date:
    TZ=America/Los_Angeles date "+%D"  | tr -d '\n' >> ~/rssihistory.txt 
    

    #Tab
    echo -e -n "\t" >> ~/rssihistory.txt

    #For time:
    TZ=America/Los_Angeles date "+%T" | tr -d '\n' >> ~/rssihistory.txt        

    #Tab
    echo -e -n "\t" >> ~/rssihistory.txt



    
    if  [[ $LINKSOUT = '' ]]
    then
	echo -n "no return from modem" >> ~/rssihistory.txt
	# run a script to reset modem somehow here...
	#sudo ifconfig eth0 down
	#sleep 5
	#sudo ifconfig eth0 up
	#sleep 5
	# Could also do:
	# sudo dhcpcd -n
    fi
    
    #For status:
    echo "$LINKSOUT" | sed -n '/4G/,/GSM/p' | sed -n '/Status:/,/Network/p' | sed '/^$/d' | sed '/Status/d' | sed '/Network/d'| tr -d ' '| tr -d '\n'  >> ~/rssihistory.txt 

    #Tab
    echo -e -n "\t" >> ~/rssihistory.txt

    
    #For strength:
    echo "$LINKSOUT" | sed -n '/4G/,/GSM/p' | sed -n '/(RSRP):/,/SNR/p' | sed 's/[^0-9,-]*//g' | sed '/^$/d' | sed  's/\r$//' | tr -d ' '| tr -d '\n'  >> ~/rssihistory.txt 

    #Tab
    echo -e -n "\t" >> ~/rssihistory.txt 
    
    #For SNR:
    echo "$LINKSOUT" | sed -n '/4G/,/GSM/p' | sed -n '/SNR:/,/Band/p' | sed 's/[^0-9,-]*//g' | sed '/^$/d' | sed 's/\r$//' | tr -d ' '| tr -d '\n'  >> ~/rssihistory.txt 

    #Tab
    echo -e -n "\t" >> ~/rssihistory.txt 

    #For band:
    echo "$LINKSOUT" | sed -n '/4G/,/GSM/p' | sed -n '/Band:/,/Roaming/p' | sed 's/[^0-9]*//g' | sed '/^$/d'| tr -d ' '| tr -d '\n'  >> ~/rssihistory.txt 

    #Tab
    echo -e -n "\t" >> ~/rssihistory.txt 

    
    #For temperature:
    /opt/vc/bin/vcgencmd measure_temp| sed 's/temp=//g'| sed 's/C//g' | tr -d \'  >> ~/rssihistory.txt 


    #    sleep 1
done    
