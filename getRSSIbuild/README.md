# Outline

This installs a script called getrssi.sh which runs at boot, to log the modem RSSI (signal strenght).
It only works if you have the USB730L model modem.

It assumes there is a 4g USB modem on eth0.

## Target hardware/prerequisites

* SD Card
* Raspberry Pi Zero W
* 4g Modem (Verizon USB730L)


## Installing

Get the Pi up and running as the other scripts have shown.

Get the script to install and configure the Pi:
```
wget https://gitlab.com/pjbca/4guav/raw/master/getRSSIbuild/installgetrssi.sh
```

Run script:
```
sudo chmod 777 ~/installgetrssi.sh; 
sudo ~/installgetrssi.sh  2>&1 | tee installgetrssibuildlog.txt 
```

Reboot Pi:
```
sudo reboot
```

Done!


## How it works:

The script installs this script: getrssi.sh.
It runs it at boot.

## What it does:

The script logs the RSSI, SNR, channel from the modem, date/time, and temperature of the Pi in file called:
~/rssihistory.txt

At first run, it renames the old rssihistory.txt file and store it in ~/logs with a filename based on the date.

## Testing:

Type

```
more ~/rssihistory.txt
```

## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

