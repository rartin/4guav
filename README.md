## Bug note:
Does not work on Buster for me. Works on Stretch. Be advised.
See:
https://github.com/raspberrypi/linux/issues/3167


## Overview

This repo is a set of scripts and instructions on how to get a 4g modem enable UAV connected to a ground control station computer.

The hardware on the UAV is a flight controller running Arudpilot software, and a Raspberry Pi Zero W with a 4g USB cellular modem.

The flight controller to Pi connection is over a UART on the Pi's serial pins.

![alt text](https://gitlab.com/pjbca/4guav/raw/master/Configuration.svg)

There are three main modules/packages:
* Mavlink-router
* Webcam
* Wifi access point to UAV

See the wiki for "under the hood" details and operational field manuals.

### Mavlink-router

This is the meat of the project, and connects the UAV to a ground computer over the internet.
It uses a cloud server to encrypt, stabilize, and coordinate all communications since a fixed IP on either end is not likely.
These instructions are for an Amazon Web Service (AWS) instance, but others should work as well.

### Webcam

An HD Pi camera can provide video, which a user on the ground can view on any browser.

### Wifi access point to UAV

For convenience, a method has been provided to connect to the AUV with the Pi's wifi.
Note this degrades stability of 4G connection in my experience, so for best stability, leave it off.
You can still use the wifi but just have to log into an external wifi router.

### GetRSSI

For convenience, a method has been provided to log the RSSI of the modem (signal strength), if you are using a USB730L model modem.


## Target hardware/prerequisites

* SD Card
* Raspberry Pi Zero W
* 4g Modem (tested on Verizon USB730L)
* Ardupilot flight controller (tested on Omnibus F4 pro V2)
* Cloud based linux instance with public IP (I used an AWS instance)
* Win10 desktop with internet access

## Prerequisite knowledge

To implement this configuration, you should be familiar with Ardupilot and how it works with UAVs, as well as the ground control station software Mission Planner.

You should also have basic familiarity with how to operate in a linux environment, how to log into a linux machine remotely using ssh, how to copy and move files, and list files in a directory.


## Installing

Follow the instructions for each package, picking which ones you want.
There is a readme for each one in its sub-directory.
The first one you have to install in mavlink-router, then webcam, the 4GAP, then getrssi (if you use USB730 4G modem) in that order. 
Otherwise, I cannot guaruntee the script will correctly configure the /etc/rc.local file.
You can look in the rclocals folder for the goal for rc.local for each of the possible configurations.

Note on 3/7/2020 I changed the order of /etc/rc.local to put the dhcpcd -n command at the end. The install scripts do not correctly order this for each package, so for now you have to manually edit /etc/rc.local after running the install scripts. I will fix the install scripts someday.


## How it works:

The UAV Pi "calls" the cloud AWS server on reverse ssh.
The Win10 ground station "calls" the AWS server on reverse ssh.

Thus, all communication is encrypted.

Both end points (UAV, Win10 ground control computer) do not need static IP addresses.


## Integration with Web CloudStation

It is possible to transmit to the CloudStation project server, described at this repo:
https://github.com/CloudStationTeam/cloud_station_web
cloned here:
https://github.com/reluctantgithubuser/cloud_station_web

There are two approaches. The easist is to modify 
/etc/mavlink-router/main.conf

and add this code:

[UdpEndpoint charlie]

Mode = Normal

Address = xyz

Port = 14550

where xyz is the IP address of the CloudStation server. The connect will be made automatically on boot of the Pi.
This has the effect of sending Mavlink traffic to two servers: The CloudStation, and the original server used in this repo.

The alternative is to manually do it. To do this, you must STOP the screen that is running mavlinkrouter, and this breaks the original IP (ssh) connection to this repo.
Then, you can manually run mavproxy using this command:

 sudo -s mavproxy.py --master=/dev/ttyAMA0 --baudrate 57600 --out xyz:14550 --aircraft MyCopter
 
 wgere again xyz is the IP address of the CloudStation server.


## Alternative webcam configuration:

While raspimjpeg is the easiest user interface, the latency can be large (up to 700 ms) for 4G connection. An alternative is to stream the bits directly out using netcat, and play them on the desktop. Here is how to do that, assuming you have a linux desktop with a monitor:
### 1) Stop raspimjpeg (on Pi  terminal)
Stop the raspimjpeg screen, if installed and running. See screen manual for insructions how to kill a screen.

### 2) SSH Pi to AWS for webcam  (on Pi terminal)
SSH into AWS for webcam traffic (sets up a reverse ssh tunnel so if you log into localhost:8901 on desktop, it will take you to AWSIPADDRESS:8080, which has the webcam data):
```
ssh -R 8080:localhost:8090 -i "AWSKEY.pem" ubuntu@AWSIPADDRESS
```
so whatever AW sees at its port 8080 goes to Pi port 8090.

### 3) Push bits out from Pi (on Pi terminal):
Push bits out from pi using netcat. Type this command on a terminal on the pi:
```
sudo raspivid -t 0 -w 1280 -h 720 -ih -vf -hf -fps 20 -b 1000000 -a 12 -o - | nc   -k -l 8090
```
OR:
```
sudo raspivid -t 0 -w 1280 -h 720 -ih -vf -hf -fps 20 -b 1000000 -a 12 -l -o tcp://0.0.0.0:8090
```
### 4) SSH desktop to AWS for webcam (on desktop terminal):
Type this command:
```
ssh -L 8901:localhost:8080 -i "AWSKEY.pem"  ubuntu@AWSIPADDRESS
```
Whatever goes to port 8901 on desktop is passed to port 8080 on AWS.
### 5) Play video stream on desktop (on desktop terminal):
mplayer should goto 127.0.0.1:8901
Type this command:
```
mplayer -fps 200 -demuxer h264es ffmpeg://tcp://127.0.0.1:8901
```
With this, results who latency over 4G of under 250 ms.

## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

 * Ardupilot
