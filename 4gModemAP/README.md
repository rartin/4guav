# Outline

This creates a wireless access point on wlan0 for local networking only.

It assumes there is a 4g USB modem on eth0.

## Target hardware/prerequisites

* SD Card
* Raspberry Pi Zero W
* 4g Modem (tested on Verizon USB730L)

For me, surprisingly, the USB730L worked on the Pi with no configuration required.
It just showed up as an eth0 interface with full internet access.

## Installing

Note: If you already have the Pi up and running, you can skip to the wget line just to get the AP up.

Download and unzip the Rasbpian OS image from:

```
https://www.raspberrypi.org/downloads/raspbian/
```
Follow the instructions here to copy the image to an sd card.

```
https://www.raspberrypi.org/documentation/installation/installing-images/README.md
```


Boot Pi with SD.

Config PI using
```
sudo raspi-config
```

* Setup keyboard
* Setup auto-login
* Enable ssh login
* Check 4g modem works: (optional)
*   Double green lights on modem should be on.
*   Lists as eth0 when running ifconfig -a
*   ping google.com
* Set up wifi for your local access point (needed to temporarily download packages)
* reboot Pi

```
sudo reboot
```

Get the script to install and configure the Pi:
```
wget https://gitlab.com/pjbca/4guav/raw/master/4gModemAP/pizeroAP.sh
```

Run script (Note you must have your 4G modem as eth0 active for the install to work):
```
sudo chmod 777 ~/pizeroAP.sh; 
sudo ~/pizeroAP.sh  2>&1 | tee pizeroAPbuildlog.txt 
```

Reboot Pi:
```
sudo reboot
```

Done!
You can now connect to the access point.


## How it works:

The script installs two packages: Hostapd and dnsmasq

Then, it configures them and creates a wireless access point on wlan0.

## Testing:

See if you can connect to the wifi access point.

Once connected, see if you can ssh to the Pi:

```
ssh pi@192.168.0.1
```

Once in, see if the Pi has internet access, e.g.:

```
ping google.com
```

## Un-installing

In my experience the dhcpcd client can get into an infinite loop when the 4G modem disconnects and reconnects to the 4G network, 
requiring reboot when this is installed. This is not always this case but can occasionally happen. 
dhcpcd just seems difficult to configure correctly for this use case.
It would be a good project to look into an alternative dhcp client service.

So the most reliable 4G connection will be with the AP NOT installed. I call this the "Tower of Babel" syndrome.

To uninstall it:

Comment these out in /etc/rc.local:
```
dhcpcd -n
iptables-restore < /etc/iptables.ipv4.nat
```


Turn off dnsmasq, hostapd (disable, stop) using this command:

```
sudo systemctl disable hostapd
sudo systemctl disable dnsmasq
```

edit /etc/dhcpcd.conf, comment out the wlan0 line, static line, and comment out the nohook line:

```
#interface wlan0
#static ip_address=192.168.0.1/24
#nohook wpa_supplicant
```

edit /etc/wpa_supplicant/wpa_supplicant.conf

add these lines
```
network={
    ssid="YOURNETWORKID"
    psk="YOURNETWORKPWD"
}
```

Edit /etc/sysctl.conf and comment

```
net.ipv4.ip_forward=1
```

Sometimes 192.168.1.1 points to wifi router and 4g modem so rssihistory is wrong unless you manually log in and disable wlan0.


## Special for Peter Burke local situation: 

I have a script which does called writeusb.sh to create the SD card. Here is what it does:
```
umount /media/peter/rootfs; 
umount /media/peter/boot ;
sudo dd bs=4M if=/home/peter/Documents/pi/2018-11-13-raspbian-stretch-lite.img of=/dev/sdf conv=fsync
```

## How to turn off wifi

In some cases, I have found the wifi signal interferes with the onboard GPS receiver. Your situation will depend on your RF environment and where you mount your components inside your plane.

To turn off wifi, edit the file /etc/rc.local with a text editor and add the following line of code (right before #START RASPIMJPEG SECTION):

```
sudo ifconfig wlan0 down
```


## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

Based off of: 
 * https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/
 * https://github.com/Phoenix1747/RouteryPi/blob/master/install.sh
