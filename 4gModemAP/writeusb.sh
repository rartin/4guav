#!/bin/bash

# Peter Burke 5/27/2019

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}


echo "Starting..."

date

umount /media/peter/rootfs; umount /media/peter/boot ; sudo dd bs=4M if=/home/peter/Documents/pi/images/2018-11-13-raspbian-stretch-lite.img of=/dev/sdf conv=fsync

date

echo "Cycle power on USB and press enter to continue when USB is remounted."
read -p "To continue press enter: " out

cp pizeroAP.sh /media/peter/rootfs/home/pi/pizeroAP.sh

umount /media/peter/rootfs; umount /media/peter/boot


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."
