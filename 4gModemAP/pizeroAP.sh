#!/bin/bash

# Peter Burke 11/2/2018
# Based off of: https://thepi.io/how-to-use-your-raspberry-pi-as-a-wireless-access-point/
# and
# https://github.com/Phoenix1747/RouteryPi/blob/master/install.sh
#
# This creates a wireless access point on WLAN0 for local networking only.
# It assumes eth0 is for internet access and already configured.
# For the application this was written for, the machine is Raspberry Pi Zero W.
# The eth0 is a USB 4g modem from Verizon, model USB730L.
# There is no communication between the two. 
# The above tutorial does to bridge the interfaces 
# does not function for this 4g modem for some reason.
#
# Therefore, all the "bridge" options are commented out, until someone can figure out how to fix.
# For now, you get an access point on wlan0 and internet access on eth0.
# 
# To save money on the 4g modem data fees, the wlan0 is used to download all the packages.
# So make sure the wlan0 is connected to the internet before running this.

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will transform your Pi into a WiFi Access Point."
read -p "To begin with the installation type in 'yes': " out

if ! [ "$out" = "yes" ]
then
  echo "You did not type in 'yes'. Exiting..."
  exit 1
fi

read -p "Please specify your SSID: " my_ssid
read -p "Please specify a password: " my_pw

if [ "$my_ssid" ] && [ "$my_pw" ]
then
    echo "SSID and password successfully entered, continuing..."
else
  echo "Empty SSID, password or country code! Exiting..."
  exit 1
fi

echo "Starting..."

date


start_time="$(date -u +%s)"


# *** needs to be brought back up at end of script
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Taking eth0 down for installation...***"
sudo ifconfig eth0 down
check_errors
echo "Done taking eth0 down for installation..."

echo "Updating package sources..."
sudo apt-get update -y
sudo apt-get upgrade -y
check_errors
echo "Done updating package sources..."

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Installing hostapd dnsmasq emacs bridge-utils..."
sudo apt-get install hostapd -y
sudo apt-get install dnsmasq -y

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
start_time_emacs="$(date -u +%s)"
#sudo apt-get install emacs -y
end_time_emacs="$(date -u +%s)"
elapsed_emacs="$(($end_time_emacs-$start_time_emacs))"
echo "Total of $elapsed_emacs seconds elapsed for emacs install process"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

sudo apt-get install bridge-utils -y
check_errors
echo "hostapd dnsmasq emacs bridge-utils successfully installed."

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Stopping hostapd and dnsmasq down for installation...***"
sudo systemctl stop hostapd
sudo systemctl stop dnsmasq
check_errors
echo "Stopped hostapd and dnsmasq down for installation..."

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Appending to /etc/dhcpcd.conf ..."
a="
interface wlan0
static ip_address=192.168.0.1/24
nohook wpa_supplicant
#denyinterfaces eth0
#denyinterfaces wlan0
"
sudo sh -c "echo '$a'>>/etc/dhcpcd.conf"
sudo chmod 777 /etc/dhcpcd.conf
check_errors
echo "Appended to /etc/dhcpcd.conf"

echo "Configuring dnsmasq"
echo "Writing to /etc/dnsmasq.conf ..."
# backup old file
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
a="
interface=wlan0
dhcp-range=192.168.0.2,192.168.0.99,255.255.255.0,24h
"
sudo sh -c "echo '$a'>/etc/dnsmasq.conf"
sudo chmod 777 /etc/dnsmasq.conf
check_errors
echo "Wrote to /etc/dnsmasq.conf"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Configuring hostapd"
echo "Writing to /etc/hostapd/hostapd.conf ..."
a="
interface=wlan0
driver=nl80211
#bridge=br0
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
ssid=SSIDCHANGEME
wpa_passphrase=PASSPHRASECHANGEME
"
sudo sh -c "echo '$a'>/etc/hostapd/hostapd.conf"

sed -i "s/SSIDCHANGEME/$my_ssid/g" /etc/hostapd/hostapd.conf
sed -i "s/PASSPHRASECHANGEME/$my_pw/g" /etc/hostapd/hostapd.conf


sudo chmod 777 /etc/hostapd/hostapd.conf
check_errors
echo "Wrote to /etc/hostapd/hostapd.conf"


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Writing to /etc/default/hostapd..."
a="
DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"
"
sudo sh -c "echo '$a'>/etc/default/hostapd"
sudo chmod 777 /etc/default/hostapd
check_errors
echo "Wrote to /etc/default/hostapd"


echo "set up traffic forwarding"
echo "Appending to /etc/sysctl.conf..."
a="
net.ipv4.ip_forward=1
"
sudo sh -c "echo '$a'>>/etc/sysctl.conf"
check_errors
echo "Appended to /etc/sysctl.conf"


echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "add a new iptables rule"

sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

echo "Appending to /etc/rc.local..."


a="
iptables-restore < /etc/iptables.ipv4.nat
exit 0
"
# delete exit on last line of /etc/rc.local
sudo sed -i '/exit 0/d'  /etc/rc.local
# append a to end
sudo sh -c "echo '$a'>>/etc/rc.local"
sudo chown root:root /etc/rc.local
sudo chmod 777 /etc/rc.local
echo "Appended to /etc/rc.local"





# need to append before the exit line

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
# new
#echo "build the bridge"

#sudo brctl addbr br0
#sudo brctl addif br0 eth0

#echo "Appending to /etc/network/interfaces..."
#a="
#auto br0
#iface br0 inet manual
#bridge_ports eth0 wlan0
#"
#sudo sh -c "echo '$a'>>/etc/network/interfaces"
#check_errors
#echo "Appended to /etc/network/interfaces"


# bring eth0 back up


echo "Bringing etho0 back up...***"
sudo ifconfig eth0 up
check_errors
echo "Brought etho0 back up...***"


echo "Bring hostapd and dnsmasq back up...***"

# https://github.com/raspberrypi/documentation/issues/1018 for BUSTER need:

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd
sudo systemctl start dnsmasq
check_errors
echo "Brought hostapd and dnsmasq back up...***"

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

date


end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."
