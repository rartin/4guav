# Outline

This installs the webcam software for you to serve up a Raspberry Pi camera on a web browser.

It then creates an ssh tunnel to an AWS instance for secure webcam traffic.

## Target hardware/prerequisites

* SD Card
* Raspberry Pi Zero W
* Raspberry Pi camera

It assumes /home/pi is your home directory.

## Installing

Download and unzip the Rasbpian OS image from:

```
https://www.raspberrypi.org/downloads/raspbian/
```
Follow the instructions here to copy the image to an sd card.

```
https://www.raspberrypi.org/documentation/installation/installing-images/README.md
```

Boot Pi with SD.

Config PI using
```
sudo raspi-config
```

* Setup keyboard
* Setup auto-login
* Enable ssh login
* Enable webcam
* Set up wifi for your local access point
* reboot Pi

```
sudo reboot
```

Get the script to install and configure the Pi:
```
wget https://gitlab.com/pjbca/4guav/raw/master/webcam/installwebcam.sh
```

Run script to set up the files so webcam starts at boot and AWS ssh tunnel configuration:
```
sudo chmod 777 ~/installwebcam.sh; 
sudo ~/installwebcam.sh  2>&1 | tee installwebcambuildlog.txt 
```
Next, run the webcam installation process (use default settings unless you want something different):
```
sudo chmod 777 /home/pi/RPi_Cam_Web_Interface/install.sh; 
sudo /home/pi/RPi_Cam_Web_Interface/install.sh   2>&1  | tee RPi_Cam_Web_Interfacebuildlog.txt
```
 Note: Just press enter enter enter... during installation to avoid changing the default settings. For me, the screen and keyboard did not behave in the configuration menu during install, but pressing enter worked for default settings.
If you for some reason need to change the defaults, I recommend to edit the config.txt file in the /home/pi/RPi_Cam_Web_Interface directory, and then run the install.sh script with the -q option. Rather than trying to use the menu of the configurator, which seems not to work for me.

Also note for Raspbian Stretch, you need php7.0 in the config.txt file, which is not the default. All the other default settings are fine.

Next, get your AWS password key file onto the Pi somehow.
Log into the AWS console for directions on how to get the .pem keyfile.
On the Pi, give it this name:
/home/pi/AWSKEY.pem
You need to set its permission carefully for AWS to accept it:
```
sudo chmod 400 /home/pi/AWSKEY.pem
```

Reboot Pi:
```
sudo reboot
```

When you first run ssh to AWS, your Pi might complain that it cannot very AWS and ask you if you want to proceed.
It only asks this once but if it is running in background you can't answer.
Therefore, run it once manually and hit yes making sure it connects, then reboot again.
```
sudo /home/pi/startupscripts/start_sshtoAWSforwebcam.sh
```
Once you enter yes and confirm it connects, just ctl-c out.

Reboot Pi again:
```
sudo reboot
```

Done!


## How it works:

The script installs a package to enable serving up the video to a browser.

The heart of the webcam is based on https://github.com/silvanmelchior/RPi_Cam_Web_Interface.
You can read about it at the wiki for that package.

In order to pass webcam traffic to a cloud server securely and behind a NAT (which we assume here to be an Amazon Web Services AWS instance although others are possible), we need an ssh.

We use a reverse tunnel, and use auto-ssh so that the Pi will "call back" to AWS if the ssh link goes down.

The ssh tunnel maps localhost:80 on the Pi(which is the webcam site) to AWSIPADDRESS:8080 on the AWS instance.
This tunnel will carry the webcam traffic. 

Therefore, to log onto the webcam from the internet, you need to log into AWSIPADDRESS:8080.

For security, the AWS instance should restrict access to AWSIPADDRESS:8080. Strageties for this are discussed in the AWS instance build section of this repo.

The build script downloads two files (from this repo) for automatically establishing and maintaining this ssh link:
* /home/pi/startupscripts/autostart_sshtoAWSforWebCam.sh (calls start_sshtoAWSforWebCam.sh as a background operation on a separate "screen")
* /home/pi/startupscripts/start_sshtoAWSforWebCam.sh (starts the reverse ssh to AWS)

It adds a line to /etc/rc.local to call aautostart_sshtoAWSforWebCam.sh on boot.

The autostart script needs the AWS password key (discussed in "install" above) and the IP address.

You set the IP address of the AWS instance when the install queries you on the command line.

It can be manually changed later by editing the start_sshtoAWSforWebCam.sh file.


## How to test it:

Log into localhost:80 or localipaddress:80 (if you are on a LAN) to see if the webcam is working.

### How to test if autostart worked on boot:
Check the list of screens:
```
screen -ls
```

There should one screen for:
* ssh to AWS for webcam

Also, there should be one ssh tunnel present on the AWS server. You can check it on the AWS server by logging into the AWS server and running this command, for example:
```
sudo tcptrack -i eth0 port 22
```

If not, you can check the logs in /home/pi/startupscripts
or you can manually try to connect (see manual testing below).

To peek at what they are doing, reattach the screen to see what is going on:
```
screen -r XYZ
```
where XYZ is the screen # from the list above.
When done inspecting, de-attach the screen:
```
ctl-a d
```

### Check all the configuration files:
The following configuration files are created/modified during the build. You can check to see if they were created properly with the correct content and permission settings:

* /etc/rc.local
* /home/pi/startupscripts/autostart_sshtoAWSforWebCam.sh
* /home/pi/startupscripts/start_sshtoAWSforWebCam.sh
* /home/pi/installwebcam.sh

## Authors

* **Peter Burke** - *Initial work* - [GitLab](https://gitlab.com/pjbca)

## License

This project is licensed under the GNU License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

 * This is based off of https://github.com/silvanmelchior/RPi_Cam_Web_Interface